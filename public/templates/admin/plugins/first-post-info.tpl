<div class="acp-page-container">
    <!-- IMPORT admin/partials/settings/header.tpl -->
    <div class="row m-0">
        <div id="spy-container" class="col-12 col-md-8 px-0 mb-4" tabindex="0">
            <form class="form first-post-info-settings">
                <h5 class="fw-bold tracking-tight settings-header">[[nodebb-plugin-first-post-info:settings_display_header]]</h5>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="displayOnFirstTopic" id="displayOnFirstTopic">
                    <label class="form-check-label" for="displayOnFirstTopic">
                        [[nodebb-plugin-first-post-info:settings_display_first_topic]]
                    </label>
                </div>
                <div class="form-check mb-3">
                    <input class="form-check-input" type="checkbox" name="displayOnFirstPost" id="displayOnFirstPost" />
                    <label class="form-check-label" for="displayOnFirstPost">
                        [[nodebb-plugin-first-post-info:settings_display_first_post]]
                    </label>
                </div>
                <h5 class="fw-bold tracking-tight settings-header">
                    [[nodebb-plugin-first-post-info:settings_display_message]]
                </h5>
                <div class="mb-3">
                    <textarea class="form-control" name="displayMessage" id="displayMessage" rows="10"></textarea>
                </div>
                <div class="mb-3">
                    <button id="previewMessage" type="button" class="btn btn-secondary">
                        [[nodebb-plugin-first-post-info:settings_message_preview]]
                    </button>
                </div>
            </form>
        </div>
        <!-- IMPORT admin/partials/settings/toc.tpl -->
    </div>
</div>
