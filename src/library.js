'use strict';

let async = require('async');

let meta = require.main.require('./src/meta');
let plugins = require.main.require('./src/plugins');

let plugin = {};

plugin.init = function (params, callback) {
    function renderAdmin(req, res) {
        res.render('admin/plugins/first-post-info', {
            title: 'First Post Info',
        });
    }

    params.router.get('/admin/plugins/first-post-info', params.middleware.admin.buildHeader, renderAdmin);
    params.router.get('/api/admin/plugins/first-post-info', renderAdmin);

    const socketPlugins = require.main.require('./src/socket.io/plugins');
    socketPlugins.firstPostInfo = {
        getInfoHtml: function(socket, params, callback) {
            async.waterfall([
                function (next) {
                    if(params.displayMessage && params.displayPreview) {
                        next(null, params);
                    } else {
                        meta.settings.get('nodebb-plugin-first-post-info', next);
                    }
                },
                function (settings, next) {
                    if(settings.displayMessage) {
                        plugins.hooks.fire('filter:parse.raw', settings.displayMessage, function (err, message) {
                            settings.displayMessage = message;
                            next(err, settings)
                        });
                    } else {
                        next(null, {});
                    }
                }
            ], callback);
        }
    };

    callback();
};

plugin.addAdminNavigation = function (custom_header, callback) {
    custom_header.plugins.push({
        route: '/plugins/first-post-info',
        icon: 'fa-info',
        name: 'First Post Info',
    });

    callback(null, custom_header);
};

module.exports = plugin;
